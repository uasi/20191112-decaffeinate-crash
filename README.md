Minimal reproducible example of https://github.com/decaffeinate/decaffeinate/issues/1622

```
$ yarn install
$ ./node_modules/.bin/decaffeinate < input.coffee 2> err.log
$ head -n30 err.log
/Users/uasi/tmp/decaf/node_modules/decaffeinate/dist/cli.js:304
        throw err;
        ^

RangeError: unknown: Maximum call stack size exceeded
    at NodePath._call (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/path/context.js:55:20)
    at NodePath.call (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/path/context.js:42:17)
    at NodePath.visit (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/path/context.js:90:31)
    at TraversalContext.visitQueue (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/context.js:112:16)
    at TraversalContext.visitSingle (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/context.js:84:19)
    at TraversalContext.visit (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/context.js:140:19)
    at Function.traverse.node (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/index.js:84:17)
    at traverse (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/index.js:66:12)
    at NodePath.traverse (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/path/index.js:148:24)
    at Scope.crawl (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/scope/index.js:639:10)
    at Scope.init (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/scope/index.js:589:32)
    at NodePath.setScope (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/path/context.js:131:30)
    at NodePath.replaceWith (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/path/replacement.js:159:8)
    at PluginPass.ObjectProperty (/Users/uasi/tmp/decaf/node_modules/@resugar/codemod-objects-concise/src/index.js:20:22)
    at newFn (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/visitors.js:179:21)
    at NodePath._call (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/path/context.js:55:20)
    at NodePath.call (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/path/context.js:42:17)
    at NodePath.visit (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/path/context.js:90:31)
    at TraversalContext.visitQueue (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/context.js:112:16)
    at TraversalContext.visitQueue (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/context.js:118:21)
    at TraversalContext.visitQueue (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/context.js:118:21)
    at TraversalContext.visitQueue (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/context.js:118:21)
    at TraversalContext.visitQueue (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/context.js:118:21)
    at TraversalContext.visitQueue (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/context.js:118:21)
    at TraversalContext.visitQueue (/Users/uasi/tmp/decaf/node_modules/@babel/traverse/lib/context.js:118:21)
```
