module.exports = {
  presets: [
    // Bad
    ["@babel/preset-env", { targets: ["IE 11"] }],
    // OK
    //["@babel/preset-env", { targets: ["last 1 Chrome version"] }],
  ],
};
